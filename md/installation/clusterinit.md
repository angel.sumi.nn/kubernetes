[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Kubernetes Cluster konfigurieren

## Task Master-Node

> **ACHTUNG:** Nachfolgende Schritte nur auf **Master-Node** ausführen

In den nächsten Schritten wird die Master-Node Installation fertig gestellt. 


#### bash auto-completion

Analog zur `Bash` kann auch mit `kubectl` mit *tab* das Kommando auto-vervollständigt werden. Um das zu aktivieren müssen nachfolgende Kommandos ausgeführt werden.  

```
# yum install bash-completion
# echo 'source <(kubectl completion bash)' >>~/.bashrc
```

#### Alias für Kubernetes

Um das Arbeiten mit `kubectl` zu erleichtern empfiehlt es sich, für wiederkehrende Kommandos *Alias* zu definieren. Diese sollen den persönlichen Bedürfnissen angepasst sein und werden im File `$HOME/.bashrc` definiert.

```
# cat <<EOF>> $HOME/.bashrc

# Alias for kubectl
alias kc='kubectl create --validate -f'
alias kg='kubectl get'
alias kgs='kubectl get -n=kube-system'
alias kga='kubectl get --all-namespaces'
alias kgall='kubectl get all --all-namespaces'
alias ks='kubectl -n kube-system'
EOF
```

Anschliessend muss `$HOME/.bashrc` sourced werden damit die Alias aktiv werden.

```
# source $HOME/.bashrc
```

#### Cluster Initialisieren

Mit `kubeadm init` Kubernetes initialisieren. Dieser Befehl erzeugt einen **Join-Token**, mit welchem die Nodes dem Cluster zugefügt werden können. 

 > TIP: Join-Token als VB-Beschreibung bei der Master-VM hinterlegen

```
# kubeadm init --apiserver-advertise-address=192.168.11.11 --pod-network-cidr=10.244.0.0/16
```
Auf den *worker nodes* wird später dieses Kommando zum Joinen verwendet.

```
# kubeadm join 192.168.11.11:6443 --token yjsrhl.jo4rxz7m9rtpojgb \
        --discovery-token-ca-cert-hash sha256:2e917b07108174e417294707d6b7b6194237dcb560173b9051d8d043453989f5
```


#### Kubernetes Environment

Für die Administration muss die Kubernets Konfiguratione in das entsprechende `$HOME` kopiert werden.

```
# mkdir -p $HOME/.kube
# cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
# chown $(id -u):$(id -g) $HOME/.kube/config
```

### Overlay Network installieren

#### Flannel
Für das Networking wird ein Overlay Netzwerk benötigt. [Flannel][4] ist dazu in den meisten Fällen gut geeignet. 

```
# mkdir $HOME/flannel
# cd $HOME/flannel
# wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml -O kube-flannel.yml
```

Da diese Installation [mehrere Interfaces][6] hat (Nat & Host-Only), muss [Flannel][4] mit dem Argument `--iface=enp0s8` erweitert werden, wobei das Interface den lokalen Bedingungen angepasst werden muss.

Dazu wird im `kube-flannel.yml` File die `args:` unter der Conatiner Section entsprechend erweitert

```
      containers:
      - name: kube-flannel
       #image: flannelcni/flannel:v0.17.0 for ppc64le and mips64le (dockerhub limitations may apply)
        image: rancher/mirrored-flannelcni-flannel:v0.17.0
        command:
        - /opt/bin/flanneld
        args:
        - --ip-masq
        - --kube-subnet-mgr
        - --iface=enp0s8
```

Nachdem die Anpassung erfolgt ist, kann [Flannel][4] deployed werden

```
# kubectl apply -f kube-flannel.yml
```

> es gibt verschiedenen Netzwerke mit denen Kubernetes arbeiten kann

Ein ebenfalls oft verwendetet *Overlay-Network* ist [weave Network][5]. Diese Konfiguration ist in diesem Beispiel allerdings nicht getestet.

[Weave Network][5] würde folgendermassen installiert werden.

```
# export kubever=$(kubectl version | base64 | tr -d '\n')
# kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
```

> Es kann nur ein Overlay Network zur selben Zeit installiert sein.


#### Master-Node testen

Der Master-Node ist nun bereit. Testen kann man die korrekte Funktion mit `kubectl get nodes`. Ist der Status *Ready*, funktioniert der Cluster.

```
# kubectl get nodes

NAME             STATUS   ROLES                  AGE    VERSION
vb-master-node   Ready    control-plane,master   4m4s   v1.23.5
```




## Worker-Node mit Master-Node joinen

Nun wird muss der Node noch mit dem *Join Befehl*, welchen wir bei der Master-Node Initialisierung erhalten haben mit dem Master-Node joined werden.

> untenstehend ist ein Beispiel, der Token muss mit dem tatsächlichen Token der Installation ersetzt werden

```
# kubeadm join 192.168.11.11:6443 --token yjsrhl.jo4rxz7m9rtpojgb \
        --discovery-token-ca-cert-hash sha256:2e917b07108174e417294707d6b7b6194237dcb560173b9051d8d043453989f5
```


#### Worker-Node testen

Wiederum mit `kubectl get nodes` testen, ob die *Worker-Nodes* korrekt joined sind. Es kann einen Moment dauern, bis alle Nodes *ready*  sind.

```
#  kubectl get nodes
NAME      STATUS   ROLES                  AGE    VERSION
master    Ready    control-plane,master   118m   v1.23.5
worker1   Ready    <none>                 113m   v1.23.5
worker2   Ready    <none>                 106m   v1.23.5
```
