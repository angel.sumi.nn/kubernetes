[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Klone für Worker-Nodes erstellen

Die bisher getätigten Installationen sind für Master- und Worker-Node identische. Daher kann für die Worker-Nodes einfachheitshalber ein Klone der bisherigen Master-Node Installation als Basis für die Worker-Node erstellt werden. 

Nachfolgend wird beschrieben, wie ab dem *Klone* der *Worker-Node* `vb-node1`  erstellt wird. Dieselben Schritte müssen für alle weiteren *Worker-Nodes* durchegführt werden.

> **ACHTUNG:** beim Klonen unbedingt neue **MAC** und **UUID** erzeugen

### Task nach dem Klonen

> **ACHTUNG:** Master-Node muss **poweroff** sein damit es keinen IP Konflikt gibt

#### Worker Nodes

| Node | domain| IP  |
|:--:|:--:|:--:|
| worker1 |k8s| 192.168.11.12 |
| worker2| k8s| 192.168.11.13 |


Folgende Task  für **jeden Worker-Node** sequenziell ausführen:

* Klone VM starten
* IP Anpassen
* Hostname anpassen
* VM rebooten

#### IP Adresse des `enp0s8` Interface anpassen.

> das Interface kann abweichend sein und muss dem *host-only* Interface entsprechen.

Zeile `IPADDR=192.168.11.11` durch `IPADDR=192.168.11.12`für **worker1**, respektive `IPADDR=192.168.11.13` für **worker2** ersetzen

```
# vi /etc/sysconfig/network-scripts/ifcfg-enp0s8

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=192.168.11.12
PREFIX=24
DNS1=192.168.10.1
IPV6_PRIVACY=no
```

#### Hostname anpassen

Hostname mit `hostnamectl`anpassen. Diesen Schritt für beide Klone, **worker1** und **worker2** ausführen.

```
# hostnamectl set-hostname worker1
```

#### Rebooten

```
# sync 
# reboot
```

## Master und Worker Node starten

Nachdem alle Klone die vorgesehene IP und Hostname gesetzt haben, können alle VM, also der neue Kubernetes Cluster, gleichzeitig gestartet werden.