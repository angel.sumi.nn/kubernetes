[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# MySQL

Eine [MySQL Instanz][90] mit pv und pvc deployen. 

Beispieldateien speichern unter `$HOME/deployment/mysql`.

```
# mkdir -p /$HOME/deployment/mysql
# cd /$HOME/deployment/mysql
```

## Volumes

In der folgenden **MySQL** Demo-Applikation wird `pvc-local-mysql` als Storage definiert. Dabei ist es der Applikation egal wo genau der Storage liegt. Nachfolgend werden zwei Varianten, einmal *lokal* und einmal über *NFS* definiert. 

Damit die *MySQL* Datenbank jeweils exklusiven Zugriff auf den Storage erhält, wird der Access entsprechend eingeschränkt.

```
accessModes:
    - ReadWriteOnce
``` 


### lokaler Storage

> Das lokale *hostPath* Verzeichnis `/mnt/data` wird automatisch auf dem Node, auf welchem der *POD* Deployed wurde erstellt.

> ACHTUNG: *hostPath* gilt als unsicher und veraltet


Im File `pv-local-mysql.yaml` wird das lokale *Persistent Volume* definiert.

```
# cat <<EOF>> $HOME/volumes/pv-local-mysql.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-local-mysql
  labels:
    type: local
spec:
  persistentVolumeReclaimPolicy: Retain
  storageClassName: mysql
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
EOF
```
Im File `pvc-local-mysql.yaml`  wird ein *Persistent Volume Claim* definiert, welcher auf das lokale PV zeigt.

```
# cat <<EOF>> $HOME/volumes/pvc-local-mysql.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-local-mysql
spec:
  storageClassName: mysql
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
EOF
```

### NFS Storage

Alternativ kann **NFS** über den vorgängig erstellten *NFS-Server*  konfiguriert werden.

Im File `pv-nfs-mysql.yaml` wird das NFS *Persistent Volume* definiert.

```
# cat <<EOF>> $HOME/volumes/pv-nfs-mysql.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-mysql
spec:
  persistentVolumeReclaimPolicy: Retain
  storageClassName: mysql
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteOnce
  nfs:
    server: nfsserver
    path: "/mnt/nfs_shares/mysql"
EOF
```

Im File `pvc-nfs-mysql.yaml`  wird ein *Persistent Volume Claim* definiert, welcher auf das lokale PV zeigt.

```
# cat <<EOF>> $HOME/volumes/pvc-nfs-mysql.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs-mysql
spec:
  storageClassName: mysql
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
EOF
```


## Deployment

Im *MySQL* Deployment wird nun der vorgängig definierte *persistentVolumeClaim* für die Storage Zuordnung definiert.

* `pvc-local-mysql` für lokalen Storag
* `pvc-nfs-mysql` für NFS Storage




Beispiel mit NFS

```
volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: pvc-nfs-mysql
```


Im File `mysql.yaml` wird das gesammte *MySQL* Deployment* inklusive *Service* definiert.

```
# cat <<EOF>> $HOME/deployment/mysql/mysql.yaml
apiVersion: v1
kind: Service
metadata:
  name: mysql
spec:
  ports:
  - port: 3306
  selector:
    app: mysql
  clusterIP: None
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
spec:
  selector:
    matchLabels:
      app: mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - image: mysql:5.6
        name: mysql
        env:
          # Use secret in real usage
        - name: MYSQL_ROOT_PASSWORD
          value: password
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: pvc-nfs-mysql
EOF
```

## Applizieren

```
# kubectl create -f $HOME/volumes/pv-nfs-mysql.yaml
# kubectl create -f $HOME/volumes/pvc-nfs-mysql.yaml
# kubectl apply -f $HOME/deployment/mysql/mysql.yaml
```

## Testen

Deploymenet auslesen

```
# kubectl describe deployment mysql
```

PODs listen

```
# kubectl get pods -l app=mysql
```

PersitentVolumeClaim listen

```
# kubectl describe pvc pvc-nfs-mysql
```

##  MySQL benutzen

Nun kann in die MySQL Datenbank eingeloggt werden

```
# kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
3 rows in set (0.02 sec)
```

## MySQL Service exposen

Den *MySQL* Zugriff von ausserhalb wird wiederum mit `kubectl expose` erstellt.

```
# kubectl expose deployment mysql --type=NodePort --name=mysql-service -n default
```

Exposed Port anzeigen

```
# kubectl get service
NAME                       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
kubernetes                 ClusterIP   10.96.0.1        <none>        443/TCP          3d8h
mysql                      ClusterIP   None             <none>        3306/TCP         14m
mysql-service              NodePort    10.97.232.188    <none>        3306:31229/TCP   13s
nginx-nfs-gold-service     ClusterIP   10.99.125.171    <none>        80/TCP           24h
nginx-nfs-platin-service   ClusterIP   10.96.250.70     <none>        80/TCP           24h
nginx-nfs-silver-service   ClusterIP   10.108.168.123   <none>        80/TCP           24h
```

Nun kann vom lokalen Host mit einem *MySQL* Client über Port *31229* auf die Datenbank zugegriffen werden.



